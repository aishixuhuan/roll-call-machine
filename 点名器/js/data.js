var personArray = new Array();
var nameList = [
  '酸辣粉',
  '酸辣土豆丝盖饭',
  '熏肉大饼',
  '齐品达',
  '旋转小火锅',
  '饺子',
  '塔斯汀汉堡',
  '炒菜',
  '沈老头包子',
  '烤冷面',
  '涮串',
  '抻面',
  '喝杯咖啡',
  '鸡公煲',
  '冒菜',
  '螺蛳粉',

  // '于岩',
  // '于滋润',
  // '刘娜',
  // '縢润升',
  // '翟樑玉',
  // '朱玉鑫',
  // '张春阳',
  // '薛艳芳',
  // '杨中超',
  // '李靖宇',
];
function initData() {
  //var $people = localStorage.getItem("peopleData");
  //if(!$people){
  //    localStorage.setItem("peopleData", nameList);
  //}else {
  //    nameList = $people.split(',');
  //}
  personArray = [];
  for (var i = 0; i < nameList.length; i++) {
    var person = {
      id: i,
      image: 'img/tx' + (i + 1) + '.jpg',
      thumb_image: 'img/tx' + (i + 1) + '.jpg',
      name: nameList[i],
    };
    personArray.push(person);
  }
}
initData();
